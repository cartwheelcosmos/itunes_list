import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // example
    },
    state:{
      current_album:{
        title:'',
        thumbnail:'',
        singer:''
      }
    },
    mutations:{
      set_current_album(state,album_data){
        state.current_album={...album_data}
      }
    },
    actions:{
      setCurrentAlbum(context,album_data){
        context.commit('set_current_album',album_data)
      }
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
